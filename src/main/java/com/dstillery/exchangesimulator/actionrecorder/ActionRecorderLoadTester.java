package com.dstillery.exchangesimulator.actionrecorder;

import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.pixel.MySqlPixelService;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.HttpUtil;
import com.dstillery.common.util.StringUtil;
import com.dstillery.exchangesimulator.loadtester.ExchangeIdService;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.Constants.NOMADINI_DEVICE_ID;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;

public class ActionRecorderLoadTester  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionRecorderLoadTester.class);
    private static final String ALL_KEY = "ALL";
    private final LoadingCache<String, List<Pixel>> pixelCache;

    private MetricReporterService metricReporterService;
    private ExchangeIdService exchangeIdService;
    private BlockingQueue<String> commonUsersBetweenBidderAndActionRecorder;
    private DiscoveryService discoveryService;

    public ActionRecorderLoadTester(
            MetricReporterService metricReporterService,
            MySqlPixelService mySqlPixelService,
            ExchangeIdService exchangeIdService,
            BlockingQueue<String> commonUsersBetweenBidderAndActionRecorder,
            DiscoveryService discoveryService) {
        this.exchangeIdService = exchangeIdService;
        this.discoveryService = discoveryService;
        this.metricReporterService = metricReporterService;
        this.commonUsersBetweenBidderAndActionRecorder = commonUsersBetweenBidderAndActionRecorder;

        pixelCache = CacheBuilder.newBuilder()
                .expireAfterAccess(10,
                        TimeUnit.MINUTES)
                .expireAfterWrite(10,
                        TimeUnit.MINUTES)
                .build(new CacheLoader<String, List<Pixel>>() {
                    @Override
                    public List<Pixel> load(String key) {
                        return mySqlPixelService.readAll();
                    }
                });
    }

    public void syncGoogleThread() {

        try {
            //we keep it low for now, because we have enough users 60000 in the cassandra and mysql
            ////LOG_EVERY_N(ERROR, 10),"syncing random users for google";

            //disable for now, we have enough pixels
            sendRequestToGooglePixelMatching(10);
            GUtil.sleepMillis(100);
        } catch(Exception e) {
            LOGGER.error("error in syncing google thread", e);
        }

    }

    private Pixel getRightPixelBasedOnDeviceId(String nomadiniDeviceId) throws SQLException, IOException, ExecutionException {
        List<Pixel> allPixels = pixelCache.get(ALL_KEY);

        long hashOfDevices = StringUtil.getHashedNumber(nomadiniDeviceId);
        //we want a pixel to have a consistent set of devices in it
        //this is done consistent with BidderLoadTester.setConsistentIabCatsPerDevicePercentageGroup
        //wich sets a consistent iab cat for devices...
        int groupOfDevice = (int) Math.abs(hashOfDevices) % (allPixels.size() - 1);

        return allPixels.get(groupOfDevice);
    }

    public void sendPixelActionToActionRecorder(int numberOfPixelActions, boolean sendSameUserSeenByBidder) {

        for (int i = 0; i < numberOfPixelActions; i++) {
            try {
                String nomadiniDeviceId;
                if (sendSameUserSeenByBidder) {
                    nomadiniDeviceId = commonUsersBetweenBidderAndActionRecorder.poll(1, TimeUnit.SECONDS);
                    LOGGER.info("common bidder action recorder device Id : {}", nomadiniDeviceId);
                } else {
                    nomadiniDeviceId = exchangeIdService.getRandomSyncedNomadiniDeviceId("google", 10000);
                }

                if (nomadiniDeviceId == null || nomadiniDeviceId.isEmpty()) {
                    LOGGER.warn("empty deviceId found, skipping the request");
                    continue;
                }

                Pixel pixel = getRightPixelBasedOnDeviceId(nomadiniDeviceId);

                String actionRecorderUrl =
                        discoveryService.discoverService("actionrecorder") + "/pixelaction?pixelId=" + pixel.getUniqueKey();
                LOGGER.info("sending nomadiniDeviceId : {} to pixel id {}, actionRecorderUrl : {}",
                        nomadiniDeviceId , pixel.getId(), actionRecorderUrl);

                //we set the cookie here and send request the action recorder
                HttpResponse response =
                        HttpUtil.sendGetRequestAndGetFullResponse(
                                ImmutableMap.of(NOMADINI_DEVICE_ID, nomadiniDeviceId),
                                actionRecorderUrl,
                                10000,
                                3);
                metricReporterService.
                        addStateModuleForEntity("sent_action_request",
                                "ActionRecorderLoadTester",
                                ALL_ENTITIES
                        );

                 String NomadiniDeviceIdCookieRead =
                         HttpUtil.getCookieFromResponse(response, NOMADINI_DEVICE_ID);
                 if (NomadiniDeviceIdCookieRead.isEmpty()) {
                         LOGGER.error("nomadiniDeviceId was empty");
                 } else {
                         LOGGER.debug("pixel action called successfully");
                 }

                 //THIS IS VERY IMPORTANT the Cookie returned must be the same as the cookie that was set in the request
                 assertAndThrow(NomadiniDeviceIdCookieRead.equals(nomadiniDeviceId));

            } catch(Exception e) {
                LOGGER.warn("exception in pixel action ", e);
            }
        }
    }

    //this method will sync 10000 random gooleIds using calling the googlePixelMatching in actionRecorder
    private void sendRequestToGooglePixelMatching(int numberOfUsers) {

        for (int i=0; i < numberOfUsers; i++) {
            try{
                String exchangeName = "google";
                String firstPart = "googleId";
                String googleId = StringUtil.randomString( firstPart, 12);
                //TODO : read the properties from actionrecorder.properties
                String actionRecorderUrl =
                        discoveryService.discoverService("actionrecorder")
                                + "/googlepixelmatching?google_gid=__NEW_GOOGLE_ID__&google_cver=1";
                actionRecorderUrl = actionRecorderUrl.replaceAll(
                                "__NEW_GOOGLE_ID__",
                                googleId);
                //LOGGER.debug("hitting actionRecorderUrl : {} ",  actionRecorderUrl;
                HttpResponse response = HttpUtil.sendGetRequestAndGetFullResponse(actionRecorderUrl,
                        10000,
                        3);
                if (response.getStatusLine().getStatusCode() != HTTP_NO_CONTENT) {
                    ////LOG_EVERY_N(ERROR, 100), google.COUNTER, "nth : response status : {} ",  response.getStatus();
                    throw new RuntimeException("response status is "+ response.getStatusLine().getStatusCode());
                }

                metricReporterService.
                        addStateModuleForEntity("matched_device_id",
                                "ActionRecorderLoadTester",
                                ALL_ENTITIES
                        );
                String nomadiniDeviceIdCookie =
                        HttpUtil.getCookieFromResponse(response, NOMADINI_DEVICE_ID);
                LOGGER.debug("synced nomadiniDeviceIdCookie : {}", nomadiniDeviceIdCookie);
                //we create a device from the cookie to make sure
                //the cookie is set correctly
                NomadiniDevice device = new NomadiniDevice(
                        nomadiniDeviceIdCookie, DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP);

                GUtil.sleepMillis(1);
            } catch(Exception e) {
                LOGGER.warn("exception in pixel matching", e);
            }
        }
    }

}


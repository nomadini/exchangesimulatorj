package com.dstillery.exchangesimulator.adserver;

import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.openrtb.beans.Bid;
import com.dstillery.openrtb.beans.BidResponse;
import com.dstillery.common.device.NomadiniDevice;

import com.dstillery.common.util.HttpUtil;
import com.dstillery.common.util.StringUtil;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;


import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.EXCEPTION;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.IMPORTANT;
import static com.dstillery.common.util.Constants.NOMADINI_DEVICE_ID;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.replaceString;
import static com.dstillery.common.util.StringUtil.toStr;

public class AdServerCallerTask implements Runnable  {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerCallerTask.class);

    private BlockingQueue<String> bidderResponses;
    private MetricReporterService metricReporterService;
    private AdServerLoadTester adServerLoadTester;
    private AtomicLong goodAdServerResponse;
    private AtomicLong syncedGoogleId;
    private Random priceRandomizer;

    public AdServerCallerTask(
            BlockingQueue<String> bidderResponses,
            MetricReporterService metricReporterService,
            AdServerLoadTester adServerLoadTester) {
        this.adServerLoadTester = adServerLoadTester;
        this.metricReporterService = Objects.requireNonNull(metricReporterService);
        this.bidderResponses = bidderResponses;
        goodAdServerResponse = new AtomicLong();
        syncedGoogleId = new AtomicLong();
        priceRandomizer = new Random ();
    }

    public void run() {
        LOGGER.info("bidderResponses : {}", bidderResponses.size());
        while (!bidderResponses.isEmpty()) {
            try {
                callAdServer(bidderResponses.take());
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }
    }

    private void callAdServer(String bidResponse) {
        if (bidResponse.isEmpty()) {
            metricReporterService.
                    addStateModuleForEntity("EMPTY_BID_RESPONSE_FROM_BIDDER",
                            "AdServerCallerTask",
                            "ALL");
            return;
        }

        try {
            metricReporterService.
                    addStateModuleForEntity("NON_EMPTY_BID_RESPONSE_FROM_BIDDER",
                            "AdServerCallerTask",
                            "ALL",
                            IMPORTANT);
            LOGGER.info("bidResponse : {}", bidResponse);
            BidResponse bidResponsePtr =
                    OBJECT_MAPPER.readValue(bidResponse, BidResponse.class);

            if (bidResponsePtr != null) {

                Bid bid = bidResponsePtr.getSeatbid().get(0).getBid().get(0);
                String nurl = bid.getIurl();
                String notificationUrl =
                        replaceString(nurl, "${AUCTION_PRICE}",
                                toStr(bid.getPrice() +
                                        priceRandomizer.nextFloat()), true);

                // notificationUrl = "http://localhost:9981/win?won=3.552052080603878&trnId=ey";

                String adserverResponse
                        = adServerLoadTester.sendWinNotificationsToAdserv(notificationUrl);

                callMatchTag(adserverResponse);
                callClickUrl(adserverResponse);
                if (adserverResponse.isEmpty() || adserverResponse.equalsIgnoreCase("NONE")) {
                    ////LOG_EVERY_N(ERROR, 1000) , google.COUNTER, "th empty bidder response";
                    metricReporterService.addStateModuleForEntity(
                            "EMPTY_AD_RESPONSE_FROM_ADSERVER",
                            "AdServerCallerTask",
                            ALL_ENTITIES
                    );

                    return;
                }

                goodAdServerResponse.getAndIncrement();
                metricReporterService.addStateModuleForEntity(
                        "GOOD_AD_RESPONSE_FROM_ADSERVER",
                        "AdServerCallerTask",
                        ALL_ENTITIES
                );

                LOGGER.info("nurl : {} , notificationUrl : {} ", nurl, notificationUrl);
                LOGGER.info("# goodAdServerResponse from adserver : {}", goodAdServerResponse.get());

                LOGGER.info("adserver response : {} ",  adserverResponse);
            }
        } catch(Exception e) {
            LOGGER.warn("exception caught", e);
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION",
                    "AdServerCallerTask",
                    ALL_ENTITIES,
                    EXCEPTION);
        }
    }

    private void callClickUrl(String adserverResponse) {
        //LOGGER.info("adserverResponse : {} ",  adserverResponse;
        // links = gumboHtmlParser.extractLinks(adserverResponse);
        // //LOGGER.info("links : " , JsonArrayUtil.convertListToJson(links);
        // //LOGGER.error("links : " , JsonArrayUtil.convertListToJson(links);

        //root@alpha1:/home/vagrant/workspace/mango# php artisan serve
        //now we create a file this this
//                List<String> links = StringUtil.searchWithRegex(adserverResponse, "href=.>");
        // //LOGGER.error("links : " , JsonArrayUtil.convertListToJson(links);
        // assertAndThrow(links.size()> 0);
        // link = links.get(0);
        // //LOGGER.error("link : " , link;
    }

    private void callMatchTag(String adserverResponse) throws Exception {
        //
        //<img src="http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm" />
        String googlePixelMatchTag = "<img src=\"http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm\" />";
        if (StringUtil.contains(adserverResponse, googlePixelMatchTag)) {
            //refer to this to see how pixel matching works
            //https://developers.google.com/ad-exchange/rtb/cookie-guide
            String firstPart = "googleId";
            String googleId = StringUtil.randomString( firstPart, 12);
            String newGoogleId = googlePixelMatchTag.replaceAll(
                    "google_nid=1234",
                    "google_nid=" + googleId);
            //TODO : read the properties from actionrecorder.properties
            String actionRecorderUrl =
                    "http://localhost:9989/googlepixelmatching?google_gid=" +
                            "dGhpcyBpcyBhbiBleGFtGxl&google_cver=1";

            HttpResponse response =
                    HttpUtil.sendGetRequestAndGetFullResponse(
                            actionRecorderUrl,
                            10000,
                            3);
            syncedGoogleId.getAndIncrement();
            metricReporterService.addStateModuleForEntity(
                    "syncedGoogleId",
                    "AdServerCallerTask",
                    ALL_ENTITIES);
            // allSyncedGoogleIds.insert(newGoogleId);

            String nomadiniDeviceIdCookie =
                    HttpUtil.getCookieFromResponse(response, NOMADINI_DEVICE_ID);

            //we create a device from the cookie to make sure
            //the cookie is set correctly
            NomadiniDevice device = new NomadiniDevice(nomadiniDeviceIdCookie, DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP);
        }
    }

}

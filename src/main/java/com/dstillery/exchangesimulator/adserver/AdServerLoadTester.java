package com.dstillery.exchangesimulator.adserver;

import com.dstillery.common.config.ConfigService;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.common.util.HttpUtil;
import com.fasterxml.jackson.core.type.TypeReference;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Random;


import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.toStr;

public class AdServerLoadTester  {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerLoadTester.class);

    private Random priceRandomizer;
    private AeroCacheService<EventLog> realTimeEventLogCacheService;
    private DiscoveryService discoveryService;

    public AdServerLoadTester(ConfigService configService,
                              MetricReporterService metricReporterService,
                              DiscoveryService discoveryService,
                              AeroCacheService<EventLog> realTimeEventLogCacheService) {
        priceRandomizer = new Random();
        this.discoveryService = discoveryService;
        this.realTimeEventLogCacheService = realTimeEventLogCacheService;
    }

    private void callConversionPixel(EventLog eventLog) {
        //conv-pix-action
        //ActionRecorderLoadTester.sendRequestToConversionHandler(
    }

    public void startLoadTestingAdServer() throws IOException {
    String eventLogInJson = FileNomadiniUtil.readFileInString("/home/vagrant/workspace/ExchangeSimulatorService/resources/sampleEventLog.txt");
        //don't run this...we need real testing now, not this load testing

            try {

                List<EventLog> eventLogs = prepareEvents(eventLogInJson);
                for (EventLog eventLog : eventLogs) {
                    startLoadTestingImpressionHandler(eventLog);
                    startLoadTestingImpTracker(eventLog);
                    startLoadTestingClickTracker(eventLog);
                    callConversionPixel(eventLog);
                }
            } catch(RuntimeException e) {
                LOGGER.warn("exception occurred", e);
                ////LOG_EVERY_N(ERROR, 1) , "error sending direct request to adserver";
            }
    }

    private List<EventLog> prepareEvents(String eventLogInJson) throws IOException {
        List<EventLog> allRawEventLogs = OBJECT_MAPPER.readValue(eventLogInJson,
                new TypeReference<List<EventLog>>(){});

        for (EventLog eventLog : allRawEventLogs) {
            int randomNumber = sudoRandomNumber(100000).intValue();
            eventLog.setEventId(eventLog.getEventId() + (toStr(randomNumber) + "-test-exch-sm"));
            realTimeEventLogCacheService.putDataInCache(eventLog);
        }

        return allRawEventLogs;
    }

    private void startLoadTestingImpressionHandler(EventLog eventLog) {
        try {
            String impWinUrl =
                    discoveryService.discoverService("adserver")
                            + "/win?won="+
                            eventLog.getWinBidPrice() + priceRandomizer.nextDouble()
                            +"&transactionId=" + eventLog.getEventId();

            //LOGGER.debug("impWinUrl : {} ", impWinUrl;

            //we set the cookie here and send request the action recorder
            HttpResponse responseStr = HttpUtil.sendGetRequestAndGetFullResponse(
                    impWinUrl,
                    10000,
                    3);
            ////LOG_EVERY_N(ERROR, 1) , " adserver creative response: {} ",  responseStr;
        } catch(RuntimeException e) {
            LOGGER.warn("exception occurred", e);
        } catch (Exception e) {
            LOGGER.warn("exception ", e);
        }
    }

    private void startLoadTestingImpTracker(EventLog eventLog) {
        try {
            String impTrackerUrl =
                    discoveryService.discoverService("adserver") +
                            "/queue0-imptrk?transactionId="+
                            eventLog.getEventId()
                            +"&CACHE_BUSTER=2956602";

            //LOGGER.debug("impTrackerUrl : {} ", impTrackerUrl;
            //we set the cookie here and send request the action recorder
            HttpResponse responseStr = HttpUtil.sendGetRequestAndGetFullResponse(
                    impTrackerUrl,
                    10000,
                    3);
            ////LOG_EVERY_N(ERROR, 1) , " adserver impTrackerUrl response: {} ",  responseStr;
        } catch (Exception e) {
            LOGGER.warn("exception occurred", e);
        }
    }

    private void startLoadTestingClickTracker(EventLog eventLog) {
        try {
            // string clickTrackerDestinationUrl = "https://servedby.flashtalking.com/click/3/57363;1633661;0;209;0/?ft_width=160&ft_height=600&url=8437998";
            String clickTrackerDestinationUrl =
                    discoveryService.discoverService("adserver") +
                            "/queue0-dest-clk?transactionId="+ eventLog.getEventId();

            String clickUrl =
                    discoveryService.discoverService("adserver") +
                            "/queue0-clk?transactionId="+
                            eventLog.getEventId()
                            +"&CACHE_BUSTER=2956602&ctrack=" + clickTrackerDestinationUrl;

            //LOGGER.debug("clickUrl : {} ", clickUrl;
            //we set the cookie here and send request the action recorder
            HttpUtil.sendGetRequestAndGetFullResponse(
                            clickUrl,
                            10000,
                            3);
            ////LOG_EVERY_N(ERROR, 1) , " adserver clickUrl response: {} ",  responseStr;
        } catch(Exception e) {
            LOGGER.warn("exception occurred", e);
        }
    }

    public String sendWinNotificationsToAdserv(String notificationUrl) {
        return HttpUtil.sendGetRequest(notificationUrl, 100, 2);
    }
}


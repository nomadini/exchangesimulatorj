package com.dstillery.exchangesimulator.bidder;

import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.toStr;

import java.io.IOException;

import com.dstillery.common.util.RandomUtil;
import com.dstillery.openrtb.beans.Banner;
import com.dstillery.openrtb.beans.BidRequest;
import com.dstillery.openrtb.beans.Device;
import com.dstillery.openrtb.beans.Geo;
import com.dstillery.openrtb.beans.Impression;
import com.dstillery.openrtb.beans.Site;
import com.dstillery.openrtb.beans.User;
import com.dstillery.common.device.DeviceTestHelper;
import com.dstillery.common.util.StringUtil;
import com.dstillery.exchangesimulator.util.SiteTestHelper;
import com.dstillery.exchangesimulator.util.UserAgentSampleProvider;
import com.google.common.collect.ImmutableList;

public class BidRequestCreator  {

    private UserAgentSampleProvider userAgentSampleProvider;
    public BidRequestCreator(UserAgentSampleProvider userAgentSampleProvider) {

        this.userAgentSampleProvider = userAgentSampleProvider;
    }

    BidRequest createBidRequest() throws IOException {

        BidRequest bidRequest = new BidRequest();
        bidRequest.setId(StringUtil.randomString(24));

        for (int i=0; i < 3; i++) {
            String randomBlockedCategory = SiteTestHelper.getRandomIABCategory();
            bidRequest.getBcat().add(randomBlockedCategory);
        }
        // bidRequest.badv.add(randomBlockedCategory);

        Impression imp = new Impression();
        imp.setId("imp" + StringUtil.randomString(12));

        imp.bidFloor = Float.parseFloat(
                toStr("1.") + toStr(sudoRandomNumber(100)));
        Banner banner = new Banner();
        banner.setW(180);
        banner.setH(150);
        banner.getApi().add(sudoRandomNumber(4).intValue() + 1);
        banner.setPos(sudoRandomNumber(7).intValue());
        banner.getBattr().add(1);
        imp.setBanner(banner);

        int randomNumber = sudoRandomNumber(1000).intValue();
        String randomCategory = SiteTestHelper.getRandomIABCategory();
        Site site = SiteTestHelper.createOrGetSite(
                SiteTestHelper.getRandomIABCategory(),
                SiteTestHelper.getRandomDomain());

        Device deviceOpenRtb = new Device();
        deviceOpenRtb.setIp(DeviceTestHelper.getRandomIpAddressFromFiniteSet());
        deviceOpenRtb.setUa(userAgentSampleProvider.getRandomUserAgentFromFiniteSet());
        deviceOpenRtb.setDeviceId("");

        // Mobile_Tablet = 1,
        // Personal_Computer = 2,
        // Connected_TV = 3,
        // Phone = 4,
        //sending request for desktop
        deviceOpenRtb.deviceType = 2;

        //LOGGER.debug("device.ua : " , deviceOpenRtb.ua , " , device.ip : " , deviceOpenRtb.ip;

        deviceOpenRtb.setGeo(new Geo());
        // # http://en.wikipedia.org/wiki/Extreme_points_of_the_United_States#Westernmost
        // top = 49.3457868 # north lat
        // left = -124.7844079 # west long
        // right = -66.9513812 # east long
        // bottom = 24.7433195 # south lat
        //boundaries of US lat

        //boundaries of US lon
        //
        deviceOpenRtb.getGeo().setLat(RandomUtil.getRandomBetweenRange(24.7433195, 49.3457868));
        deviceOpenRtb.getGeo().setLon(RandomUtil.getRandomBetweenRange(-124.7844079, -66.9513812));

        deviceOpenRtb.getGeo().setCountry(toStr("US"));
        deviceOpenRtb.getGeo().setCity(toStr("NEW YORK"));
        deviceOpenRtb.getGeo().setRegion(toStr("NEW YORK"));
        deviceOpenRtb.getGeo().setZip(toStr("11102"));

        User user = new User();
        user.setBuyeruid("user1000000000000");
        bidRequest.setUser(user);
        bidRequest.setImp(ImmutableList.of(imp));
        bidRequest.setSite(site);
        bidRequest.setDevice(deviceOpenRtb);

        return bidRequest;

    }

}


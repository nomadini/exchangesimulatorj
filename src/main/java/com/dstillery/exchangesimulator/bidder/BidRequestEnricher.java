package com.dstillery.exchangesimulator.bidder;//
// Created by Mahmoud Taabodi on 2/12/16.

import com.dstillery.common.creative.CreativeAdType;
import com.dstillery.openrtb.beans.Banner;
import com.dstillery.openrtb.beans.BidRequest;
import com.dstillery.openrtb.beans.Site;
import com.dstillery.common.whiteListedBiddingDomains.GlobalWhiteListEntry;
import com.dstillery.common.whiteListedBiddingDomains.MySqlGlobalWhiteListService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgBwlist.TargetGroupBWListCacheService;
import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.exchangesimulator.loadtester.ExchangeIdService;
import com.dstillery.exchangesimulator.util.SiteTestHelper;
import com.dstillery.exchangesimulator.util.UserAgentSampleProvider;
import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

/**
 this class is used by BidderLoadTester to customize a bid request
 */
//
// Created by Mahmoud Taabodi on 2/12/16.
//
public class BidRequestEnricher  {

    private List<GlobalWhiteListEntry> allWhiteListedEntries;
    private List<Banner> banners = new ArrayList<>();
    private TargetGroupBWListCacheService targetGroupBWListCacheService;
    private ExchangeIdService exchangeIdService;
    private UserAgentSampleProvider userAgentSampleProvider;
    private List<Site> known = new ArrayList<Site> ();
    private List<Site> unknown = new ArrayList<>();

    public BidRequestEnricher(MySqlGlobalWhiteListService mySqlGlobalWhiteListService,
                              TargetGroupBWListCacheService targetGroupBWListCacheService,
                              UserAgentSampleProvider userAgentSampleProvider,
                              ExchangeIdService exchangeIdService) throws IOException, SQLException {
        this.exchangeIdService = exchangeIdService;
        this.targetGroupBWListCacheService = targetGroupBWListCacheService;
        allWhiteListedEntries = mySqlGlobalWhiteListService.readAll();
        this.userAgentSampleProvider = userAgentSampleProvider;

        //we want to log all whitelisted domains in a file, for testing
        if (!FileNomadiniUtil.checkIfFileExists("/tmp/all-whitelisted-domains.txt")) {
            for (GlobalWhiteListEntry domain : allWhiteListedEntries) {
                FileNomadiniUtil.appendALineToFile("/tmp/all-whitelisted-domains.txt",
                        domain.getDomainName()+"\n");
            }
        }

        for (int i=0; i < 20; i++) {
            Banner banner1 = new Banner();
            banner1.setW(320);
            banner1.setH(50);

            Banner banner2 = new Banner();
            banner2.setW(300);
            banner2.setH(50);

            Banner banner3 = new Banner();
            banner3.setW(320); banner3.setH(90);

            banners.add(banner1);
            banners.add(banner2);
            banners.add(banner3);
        }

        Banner bannerUnMatchSize = new Banner();
        bannerUnMatchSize.setW(160);
        bannerUnMatchSize.setH(600);
        banners.add(bannerUnMatchSize);
    }

    String getAWhiteListedGlobalDomain() {

        assertAndThrow(!allWhiteListedEntries.isEmpty());

        int randIndex = sudoRandomNumber(allWhiteListedEntries.size() - 1).intValue();
        String domainName = allWhiteListedEntries.get(randIndex).getDomainName();
        assertAndThrow(domainName != null);
        return domainName;
    }

    String getARandomBadDomain() {

        assertAndThrow(!allWhiteListedEntries.isEmpty());
        int randIndex = sudoRandomNumber(allWhiteListedEntries.size() - 1).intValue();
        String goodDomain = allWhiteListedEntries.get(randIndex).getDomainName();
        goodDomain += "-bad";
        return goodDomain;
    }

    public CreativeAdType getRandomCreativeAdType() {
        List<CreativeAdType> adTypes = Arrays.asList(CreativeAdType.values());
        int randIndex = sudoRandomNumber(adTypes.size() - 1).intValue();
        return adTypes.get(randIndex);
    }

    //valid device types are defined in Device.h and they are from 1 to 7
    int getRandomDeviceType() {
        int randIndex = sudoRandomNumber(6).intValue();
        return randIndex + 1;
    }

    String getRandomCreativeAPI() {
        List<String> apis = ImmutableList.of("VPAID1.0", "VPAID2.0", "MRAID-1", "ORMMA", "MRAID-2");
        int randIndex = sudoRandomNumber(apis.size() - 1).intValue();
        return apis.get(randIndex);
    }

    List<Site> getKnownSites() {
        if (known.isEmpty()) {
            for (int i=0; i < 100; i++) {
                known.add(
                        SiteTestHelper.createOrGetSite(
                                SiteTestHelper.getRandomIABCategory(),
                                SiteTestHelper.getRandomDomain()));
            }

        }
        return known;
    }

    List<Site> getUnKnownSites() {
        if (unknown.isEmpty()) {
            for (int i=0; i < 1000; i++) {
                unknown.add(SiteTestHelper.createOrGetSite(
                        SiteTestHelper.getRandomIABCategory(),
                        SiteTestHelper.getRandomDomain()));
            }

        }
        return unknown;
    }

    void setWhiteListedSite(BidRequest bidRequest) {
        int size = targetGroupBWListCacheService.getWhiteListedDomainToListOfTargetGroups().size();
        if (size <= 0) {
            ////LOG_EVERY_N(ERROR, 1000), "size of whiteListedDomainToListOfTargetGroups is zero..fix this";
            return;
        }
        int random = sudoRandomNumber(size).intValue();
        if (random > 1) {
            if (targetGroupBWListCacheService.getWhiteListedDomainsAssignedToTargetGroups().isEmpty()) {
                //LOGGER.debug("there is no white list assigned ";
                return;
            }
            String whiteList = targetGroupBWListCacheService.getWhiteListedDomainsAssignedToTargetGroups().get(random - 1);
            bidRequest.setSite(SiteTestHelper.createOrGetSite(SiteTestHelper.getRandomIABCategory(),
                    whiteList));
        }
    }

    void setBlackListedSite(BidRequest bidRequest) {
        int size = targetGroupBWListCacheService.getWhiteListedDomainToListOfTargetGroups().size();
        int random = sudoRandomNumber(size).intValue();
        if (random >= 1) {
            if (targetGroupBWListCacheService.getBlackListedDomainsAssignedToTargetGroups().isEmpty()) {
//                VLOG(0) ,"there is no black list assigned ";
                return;
            }
            bidRequest.setSite(SiteTestHelper.createOrGetSite(SiteTestHelper.getRandomIABCategory(),
                    targetGroupBWListCacheService.
                            getBlackListedDomainsAssignedToTargetGroups().get
                            (random - 1)));
        } else {
            //LOGGER.debug("random is less than 1 : " , random;
        }

    }

    void setKnownBuyerId(BidRequest bidRequest,
                         String exchangeName) throws IOException {

        bidRequest.getDevice().setUa(userAgentSampleProvider.getRandomUserAgentFromFiniteSet());
        bidRequest.getUser().setBuyeruid(exchangeIdService.getRandomSyncedNomadiniDeviceId(exchangeName, 10000));
    }

    void setNoBuyerId(BidRequest bidRequest) {
        bidRequest.getUser().setBuyeruid("");
    }

    Banner getRandomBanner() {

        int randIndex = sudoRandomNumber(banners.size() - 1).intValue();
        return banners.get(randIndex);
    }

    void setRandomCreativeSize(BidRequest bidRequest) {
        Banner banner = getRandomBanner();

        bidRequest.getImp().get(0).getBanner().setW(banner.getW());
        bidRequest.getImp().get(0).getBanner().setH(banner.getH());
    }

    void setBlockedAdvertiser(BidRequest bidRequest) {
        List<Site> sites = getKnownSites();

        int randIndex = sudoRandomNumber(sites.size() - 1).intValue();

        bidRequest.getBadv().add(sites.get(randIndex).getPage());
    }

}

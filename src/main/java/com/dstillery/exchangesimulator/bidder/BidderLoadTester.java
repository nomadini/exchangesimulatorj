package com.dstillery.exchangesimulator.bidder;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.net.HttpClientService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.GUtil;
import com.dstillery.openrtb.beans.BidRequest;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.device.DeviceTestHelper;

import com.dstillery.common.util.StatisticsUtil;
import com.dstillery.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.GUtil.sleepMillis;
import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;

public class BidderLoadTester {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidderLoadTester.class);
    private final StatisticsUtil statisticsUtil;

    private BlockingQueue<String> requestBodies;
    private BlockingQueue<String> bidderResponses;
    private BidRequestCreator bidRequestCreator;
    private HttpClientService bidderHttpClientService;
    private BidRequestEnricher bidRequestEnricher;
    private ConfigService configService;
    private MetricReporterService metricReporterService;
    private BlockingQueue<String> commonUsersBetweenBidderAndActionRecorder;
    private String exchangeName;
    private int numberOfRequests;
    private DiscoveryService discoveryService;

    public BidderLoadTester(BidRequestEnricher bidRequestEnricher,
                            BidRequestCreator bidRequestCreator,
                            BlockingQueue<String> bidderResponses,
                            ConfigService configService,
                            MetricReporterService metricReporterService,
                            String exchangeName,
                            int numberOfRequests,
                            DiscoveryService discoveryService,
                            BlockingQueue<String> commonUsersBetweenBidderAndActionRecorder) {

        this.commonUsersBetweenBidderAndActionRecorder = commonUsersBetweenBidderAndActionRecorder;
        this.bidRequestEnricher = bidRequestEnricher;
        this.bidRequestCreator = bidRequestCreator;
        this.exchangeName = exchangeName;
        this.numberOfRequests = numberOfRequests;
        this.configService = configService;
        this.bidderResponses = bidderResponses;
        requestBodies = new ArrayBlockingQueue<>(numberOfRequests);
        this.discoveryService = discoveryService;
        statisticsUtil = new StatisticsUtil();
        this.metricReporterService = metricReporterService;
    }

    public HttpClientService getClient() {
        if (bidderHttpClientService == null) {
            String hostUrl = discoveryService.discoverService("bidder");
            bidderHttpClientService =
                    new HttpClientService(configService.getAsInt("bidder.timeoutInMillis"), hostUrl);
        }
        return bidderHttpClientService;
    }

    public void prepareRequests() {
        LOGGER.trace("prepareRequests : {}", numberOfRequests);
        for (int i = 0; i < numberOfRequests; i++) {
            try {
                BidRequest requestBody = createARequest(exchangeName);
                String requestBodyShared = OBJECT_MAPPER.writeValueAsString(requestBody);
                LOGGER.trace("requestBodyShared : {}", requestBodyShared);
                requestBodies.offer(requestBodyShared);
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }

        LOGGER.trace("requestBodies.size() : {}" , requestBodies.size());

    }

    private BidRequest createARequest(String exchangeName) throws IOException {
        BidRequest bidRequest =
                bidRequestCreator.createBidRequest ();

        bidRequest.getDevice().setIp(DeviceTestHelper.getRandomIpAddressFromFiniteSet());
        bidRequest.getSite().setDomain(bidRequestEnricher.getAWhiteListedGlobalDomain());
        if (statisticsUtil.happensWithProb(0.001)) {
            bidRequest.getSite().setDomain(bidRequestEnricher.getARandomBadDomain());
        }

        bidRequestEnricher.setBlockedAdvertiser(bidRequest);
        bidRequestEnricher.setRandomCreativeSize(bidRequest);
        bidRequestEnricher.setKnownBuyerId(bidRequest, exchangeName);
        commonUsersBetweenBidderAndActionRecorder.offer(bidRequest.getUser().getBuyeruid());
        //0.001 of requests come from unknown users
        if (statisticsUtil.happensWithProb(0.001)) {
            bidRequestEnricher.setNoBuyerId(bidRequest);
        }



        if (statisticsUtil.happensWithProb(0.2)) {

            bidRequestEnricher.setWhiteListedSite(bidRequest);
        }

        if (statisticsUtil.happensWithProb(0.2)) {
            // bidRequestEnricher.setBlackListedSite(bidRequest); //throws some exception for some reason
        }

        setConsistentIabCatsPerDevicePercentageGroup(bidRequest);

        return bidRequest;
    }

    /*
       if a deviceid hash is divisbale by 1 , it gets iab cat 1
       if a deviceid hash is divisbale by 2 , it gets iab cat 2
       and so on.
       we are doing this to make a group of devices have a certain IAB Cat and SUB CATs for clustering
       purposes
     */
    private void setConsistentIabCatsPerDevicePercentageGroup(
            BidRequest bidRequest) {
        long hashOfBidRq = StringUtil.getHashedNumber(bidRequest.getUser().getBuyeruid());
        int siteCat = (int) Math.abs(hashOfBidRq % 10  + 1);
        String iabCategory1 =
                "IAB" + siteCat;
        String iabCategory2 =
                "IAB" + siteCat + 1;
        //we add another category to the site for this device always
        //to make one device have 2 categories, in order to find the right clusters in
        //Clusterer
        bidRequest.getSite().getCat().add(iabCategory1);
        bidRequest.getSite().getCat().add(iabCategory2);

    }

    public void sendRequestsToBidder() throws InterruptedException {
        LOGGER.debug("sendRequestsToBidder");
        String bidderFinalUrl = "/bid/__exchangeName__/openrtb2.3";
        bidderFinalUrl = bidderFinalUrl.replaceAll("__exchangeName__", exchangeName);

        AtomicLong numberOfSuccessfulRequestsSent = new AtomicLong();        //this value shouldn't be that high, it blocks us from
        //calling adserver
        while (!requestBodies.isEmpty()) {
            String requestBody = requestBodies.take();

            if (GUtil.allowedToCall(100)) {
                LOGGER.debug("hitting bidder with requestBody : bidderFinalUrl: {}, {}", bidderFinalUrl, requestBody);
            }
            try {
                String response = getClient().sendPostRequestWithJsonBody(
                        bidderFinalUrl,
                        requestBody,
                        HttpClientService.PathOption.RELATIVE);

                if (GUtil.allowedToCall(100)) {
                    LOGGER.info("bidderResponse size : {}, bidder response : {}", bidderResponses.size(), response);
                }

                if (!response.isEmpty()) {
                    bidderResponses.put(response);

                }
            } catch (Exception e) {
                if (GUtil.allowedToCall(1000)) {
                    LOGGER.warn("exception happened : {}", e.getMessage());
                }
            } finally {
                sleepMillis(configService.getAsInt("bidder.sleepInMillisAfterEachCall"));
            }

        }

        metricReporterService.addStateModuleForEntityWithValue(
                "bid_request",
                requestBodies.size(),
                "BidderLoadTester",
                ALL_ENTITIES
        );

        metricReporterService.addStateModuleForEntityWithValue(
                "successful_brq_sent",
                numberOfSuccessfulRequestsSent.get(),
                "BidderLoadTester",
                ALL_ENTITIES
        );
    }

    void createRealBlockingBRQ_Strategy(BidRequest bidRequest) {
        setABlockedWebSiteForBidRequest (bidRequest);
    }

    void createSomeBlockingBRQ_Strategy(BidRequest bidRequest) {
        if (sudoRandomNumber (80) > 20) {
            //in 25% of times we see that bid requests are from blocking websites
            setABlockedWebSiteForBidRequest (bidRequest);
        }
    }

    void setABlockedWebSiteForBidRequest(BidRequest bidRequest) {

    }
}


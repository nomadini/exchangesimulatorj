package com.dstillery.exchangesimulator.devicefeaturehistory_population;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.config.ConfigService;
import com.dstillery.common.ClientAppDataReloadService;
import com.dstillery.common.audience.Domain;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.feature.Feature;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.exchangesimulator.loadtester.ExchangeIdService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class DeviceFeatureHistoryPopulator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceFeatureHistoryPopulator.class);

    private ExchangeIdService exchangeIdService;
    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;
    private MetricReporterService metricReporterService;
    private CacheService<Interest> interestCacheService;
    private Integer numberOfDevicesToInsertForEachDomain;
    private ClientAppDataReloadService clientAppDataReloadService;
    private List<Long> featureIdIgnoreList = ImmutableList.of(70L);
    public DeviceFeatureHistoryPopulator(DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
                                         MetricReporterService metricReporterService,
                                         ExchangeIdService exchangeIdService,
                                         CacheService<Interest> interestCacheService,
                                         ClientAppDataReloadService clientAppDataReloadService,
                                         ConfigService configService) {
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
        this.metricReporterService = metricReporterService;
        this.exchangeIdService = exchangeIdService;
        this.interestCacheService = interestCacheService;
        this.clientAppDataReloadService = clientAppDataReloadService;
        this.numberOfDevicesToInsertForEachDomain = configService.getAsInt(
                "deviceFeatureHistoryPopulator.numberOfDevicesToInsertForEachDomain");
        ExecutorService pool = Executors.newCachedThreadPool();
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(10);
        threadPool.scheduleAtFixedRate(() -> {
            try {

                List<Future<?>> allTasks = new ArrayList<>();
                //INCREASE the number of tasks to increase the load on bidder
                for (int i = 0 ; i <  configService.getAsInt(
                        "deviceFeatureHistoryPopulator.numberOfTasksToInsertFeatureHistory"); i++) {
                    Future<?> task = pool.submit(this::insert);
                    allTasks.add(task);
                }
                allTasks.forEach(task -> {
                    try {
                        task.get();
                    } catch (InterruptedException | ExecutionException e) {
                        LOGGER.warn("exception occurred", e);
                    }
                });
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }, 0 , configService.getAsInt("deviceFeatureHistoryPopulator.intervalInMiliSeconds"), TimeUnit.MILLISECONDS);
    }


    public void insert() {
        if (clientAppDataReloadService.isReloadProcessHealthy().get()) {
            writeSampleInterestAsHistory();
        }
    }

    public void writeSampleInterestAsHistory() {
        List<DeviceFeaturePair> featureHistoryList = new ArrayList<>();
        List<String> allDomainNames = interestCacheService.getAllEntities().stream()
                .map(Interest::getDomains)
                .flatMap(Collection::stream)
                .map(Domain::getName)
                .collect(Collectors.toList());
        allDomainNames.forEach(domain -> {


            for (int i = 0; i < numberOfDevicesToInsertForEachDomain; i++) {

                NomadiniDevice device =
                        new NomadiniDevice(exchangeIdService.getRandomSyncedNomadiniDeviceId("google", 10000),
                                DeviceTypeValue.DISPLAY,
                                DeviceClassValue.DESKTOP);

                getFeatures(device, domain).ifPresent(featureHistoryList::add);

                metricReporterService.getCounter("feature_written",
                        ImmutableMap.of("name", domain)).inc();
            }
            if (GUtil.allowedToCall(100)) {
                LOGGER.debug("100th writing {} device feature history in db : {} ",
                        featureHistoryList.size(), featureHistoryList);
            }
            if (!featureHistoryList.isEmpty()) {
                deviceFeatureHistoryCassandraService.writeBatchData(featureHistoryList);
            }

        });

    }

    private Optional<DeviceFeaturePair> getFeatures(NomadiniDevice device, String domain) {
        return domainToFeatureHistory(device, domain);
    }

    private Optional<DeviceFeaturePair> domainToFeatureHistory(NomadiniDevice device, String domain) {
        DeviceFeaturePair responseObject = new DeviceFeaturePair(
                NomadiniDevice.getDeviceFromCompositeDeviceId(
                        device.getCompositeDeviceId(),
                        device.getDeviceClass()));

        long timeOfVisit = Instant.now().toEpochMilli();

        if (isDomainRandomlyQualified() && !domain.isEmpty()) {
            String featureType = FeatureType.GTLD.name();
            Feature feature = null;
            try {
                feature = new Feature(
                        FeatureType.valueOf(featureType),
                        domain,
                        Feature.FeatureStatus.UNKNOWN);
            } catch (Exception e) {
                LOGGER.warn("can't create feature from : {}", domain);
                return Optional.empty();
            }
            FeatureHistory featureHistory = new FeatureHistory(feature, timeOfVisit);

            responseObject.setFeatureHistory(featureHistory);
            return Optional.of(responseObject);
        } else {
            return Optional.empty();
        }
    }

    private boolean isDomainRandomlyQualified() {
        return RandomUtil.sudoRandomNumber(1000) % 4 == 0;
    }
}

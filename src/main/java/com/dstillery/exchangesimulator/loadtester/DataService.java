package com.dstillery.exchangesimulator.loadtester;

import com.dstillery.common.bwlist.BWEntry;
import com.dstillery.common.bwlist.BWList;
import com.dstillery.common.bwlist.ListType;
import com.dstillery.common.bwlist.MySqlBWEntryService;
import com.dstillery.common.bwlist.MySqlBWListService;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeTestHelper;
import com.dstillery.common.creative.MySqlCreativeService;
import com.dstillery.common.whiteListedBiddingDomains.MySqlGlobalWhiteListService;
import com.dstillery.exchangesimulator.bidder.BidRequestEnricher;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;

public class DataService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataService.class);

    private BidRequestEnricher bidRequestEnricher;
    private MySqlBWEntryService mySqlBWEntryService;
    private MySqlBWListService mySqlBWListService;
    private MySqlGlobalWhiteListService mySqlGlobalWhiteListService;

    public DataService(
            MySqlBWEntryService mySqlBWEntryService,
            MySqlBWListService mySqlBWListService,
            MySqlGlobalWhiteListService mySqlGlobalWhiteListService,
            BidRequestEnricher bidRequestEnricher) {
        this.bidRequestEnricher = bidRequestEnricher;
        this.mySqlGlobalWhiteListService = mySqlGlobalWhiteListService;
        this.mySqlBWListService = mySqlBWListService;
        this.mySqlBWEntryService = mySqlBWEntryService;
    }

    List<Creative> insertCreatives(MySqlCreativeService mySqlCreativeService,
                                   String uniqueName,
                                   Long advertiserId) throws JsonProcessingException {
        List<Creative> allCreativesInserted = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            Creative creative = CreativeTestHelper.createSampleCreative();
            assertAndThrow(creative.getId () > 0);

            creative.setAdType(bidRequestEnricher.getRandomCreativeAdType());
            creative.setAdvertiserId(advertiserId);
            // creative.setApis(new ArrayList<String> (CollectionUtil.convertToList(
            //                                                                       bidRequestEnricher.getRandomCreativeAPI())));

            // creative.setSize(bidRequestEnricher.getRandomCreativeSize());
            mySqlCreativeService.insert(creative);
            allCreativesInserted.add(creative);
        }

        return allCreativesInserted;
    }

    Long insertBwList(String uniqueName, ListType listType, long clientId) throws JsonProcessingException, SQLException {
        BWList bwList = new BWList();
        String uniqueNameForList = toStr(sudoRandomNumber(10000000));

        String listName = toStr(listType);
        listName += uniqueNameForList;
        listName += uniqueName;

        bwList.setName(listName);
        bwList.setType(listType);//whitelist or blacklist
        bwList.setClientId(clientId);
        mySqlBWListService.insert(bwList);
        assertAndThrow(bwList.getId() > 0);
        for (int i=0; i < 5; i++) {
            BWEntry entry = new BWEntry();

            String domainName = "abc";
            domainName += toStr(sudoRandomNumber(10000000));
            domainName += toStr(i);
            domainName += toStr(".com");
            entry.setDomainName(domainName);
            entry.setBwListId (bwList.getId ());
            mySqlBWEntryService.insert(entry);
            assertAndThrow(entry.getId () > 0);
        }
        return bwList.getId();
    }

    void insertRandomGlobalWhiteListDomains() {
        List<String> goodDomains = ImmutableList.of(
                "abc.com", "cnn.com", "foxnew.com",
                "balatarin.com", "facebook.com", "twitter.com",
                "pokemon.com", "immigration.com", "bostonGlobe.com", "vanityFair.com");
        for (int i=0; i < goodDomains.size(); i++) {
            mySqlGlobalWhiteListService.insert(goodDomains.get(i));
        }
    }
}


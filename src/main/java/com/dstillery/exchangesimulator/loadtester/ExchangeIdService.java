package com.dstillery.exchangesimulator.loadtester;

import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.partnermap.NomadiniIdToPartnerId;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class ExchangeIdService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeIdService.class);
    private static final int STABLE_SAMPLE = 1;

    private int numberOfSyncedIdsToRead;
    private Map<String, List<String>> exchangeToSyncedIds;
    private NomadiniIdToExchangeIdsMapCassandraService nomadiniIdToExchangeIdsMapCassandraService;

    public ExchangeIdService(
            NomadiniIdToExchangeIdsMapCassandraService nomadiniIdToExchangeIdsMapCassandraService,
            int numberOfSyncedIdsToRead) {
        this.numberOfSyncedIdsToRead = numberOfSyncedIdsToRead;
        this.nomadiniIdToExchangeIdsMapCassandraService = nomadiniIdToExchangeIdsMapCassandraService;
        exchangeToSyncedIds = new ConcurrentHashMap<>();
    }

    public String getRandomSyncedNomadiniDeviceId(String exchangeName, int firstNDevices) {

        if (!exchangeToSyncedIds.keySet().contains(exchangeName)) {
            exchangeToSyncedIds.put(exchangeName, new ArrayList<>());
        }

        if (exchangeToSyncedIds.isEmpty()) {
            //LOGGER.error("returning without any id";
            return "";
        }

        List<String> devices = exchangeToSyncedIds.get(exchangeName);

        //we get the random number from 0 to 99
        //user 0 to 99 used for action taker devices
        if (exchangeToSyncedIds.isEmpty()) {
            //LOGGER.error("returning without any id";
            return "";
        }
        //we get the random number from 0 to 1000
        //user 0 to 10000 are used for bidding
        int size = devices.size() - 1;
        if (size <= 0) {
            LOGGER.error("empty device returned size of ids available: {} ",  size);
            return "";
        }

        int randomNumber = sudoRandomNumber(size).intValue();
        if (randomNumber > firstNDevices - 1) {
            randomNumber = sudoRandomNumber(firstNDevices).intValue();
        }
        if (randomNumber >= devices.size()) {
            LOGGER.error("empty device returned");

            return "";
        }
        LOGGER.trace("randomNumber : {}", randomNumber);
        String randomSyncedNomadiniDeviceId = devices.get(randomNumber);

        List<String> exchangeIdAndNomadiniDeviceIdVector = StringUtil.tokenizeString(randomSyncedNomadiniDeviceId, "___");

        if (exchangeIdAndNomadiniDeviceIdVector.size() != 2) {
            LOGGER.error("bad exchangeIdAndNomadiniDeviceIdVector pair : {} ", randomSyncedNomadiniDeviceId);
        }
        assertAndThrow(exchangeIdAndNomadiniDeviceIdVector.size() == 2);
        randomSyncedNomadiniDeviceId = exchangeIdAndNomadiniDeviceIdVector.get(1);
        LOGGER.trace("randomSyncedNomadiniDeviceId : {} , randomNumber : {}",  randomSyncedNomadiniDeviceId, randomNumber);
        return randomSyncedNomadiniDeviceId;
    }

    void setup() {
        exchangeToSyncedIds.put("google", new ArrayList<>());
        exchangeToSyncedIds.put("rubicon", new ArrayList<>());
    }

    void refreshSyncedIdsFromMySql() {
        //wait 3 seconds for cassandra to come up
        GUtil.sleepInSeconds(3);

        Set<NomadiniIdToPartnerId> allSyncedIdMaps = new HashSet<>();
        // we refresh the sync
        //we retry until we have data
         do {
            try {
                List<NomadiniIdToPartnerId> allReadIds = new ArrayList<>();
                nomadiniIdToExchangeIdsMapCassandraService.readWithPaging(allReadIds::add);
                allSyncedIdMaps.addAll(allReadIds);
                GUtil.sleepInSeconds(1);
            } catch (RuntimeException e) {
                LOGGER.warn("exception occurred", e);
                GUtil.sleepInSeconds(1);
            }
            LOGGER.info("read more data to fill the syncIds map " +
                    "with stable sample : allSyncedIdMaps.size() : {} vs numberOfSyncedIdsToRead : {}",
                    allSyncedIdMaps.size(), numberOfSyncedIdsToRead);
        } while (allSyncedIdMaps.size() <= numberOfSyncedIdsToRead);

        exchangeToSyncedIds.clear();
        LOGGER.info("refreshing exchangeSyncedIds");

        LOGGER.info("loaded allSyncedIdMaps from cassandra {}", allSyncedIdMaps.size());
        for (NomadiniIdToPartnerId nomadiniIdToPartnerId : allSyncedIdMaps) {
                String exchangeName = nomadiniIdToPartnerId.getExchangeName();
                String exchangeId = nomadiniIdToPartnerId.getExchangeId();
                String nomadiniDeviceId = nomadiniIdToPartnerId.getNomadiniDeviceId();

            if (!exchangeToSyncedIds.keySet().contains(exchangeName)) {
                exchangeToSyncedIds.put(exchangeName, new ArrayList<>());
            }
            exchangeToSyncedIds.get(exchangeName).add(exchangeId + "___" + nomadiniDeviceId);
        }

        for (Entry<String, List<String>> pair : exchangeToSyncedIds.entrySet()) {
            String exchangeName = pair.getKey();
            LOGGER.info("loaded  {} synced ids from mysql for exchange {}",
                    pair.getValue().size(),
                    exchangeName);
        }
    }

}


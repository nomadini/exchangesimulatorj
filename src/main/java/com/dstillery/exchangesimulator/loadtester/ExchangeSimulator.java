package com.dstillery.exchangesimulator.loadtester;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySource("classpath:exchangesimulator.properties")
@Import({ExchangeSimulatorEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        ExchangeSimulatorConfig.class})
public class ExchangeSimulator {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Arrays.asList(ExchangeSimulator.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

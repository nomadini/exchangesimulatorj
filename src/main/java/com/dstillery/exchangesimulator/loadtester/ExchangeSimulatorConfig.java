package com.dstillery.exchangesimulator.loadtester;

import com.dstillery.common.config.ConfigService;
import com.dstillery.common.ClientAppDataReloadService;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.interest.Interest;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.pixel.MySqlPixelService;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.targetgroup.TargetGroupCacheService;
import com.dstillery.common.whiteListedBiddingDomains.MySqlGlobalWhiteListService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgBwlist.TargetGroupBWListCacheService;
import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.exchangesimulator.actionrecorder.ActionRecorderLoadTester;
import com.dstillery.exchangesimulator.adserver.AdServerLoadTester;
import com.dstillery.exchangesimulator.bidder.BidRequestCreator;
import com.dstillery.exchangesimulator.bidder.BidRequestEnricher;
import com.dstillery.exchangesimulator.bidder.BidderLoadTester;
import com.dstillery.exchangesimulator.devicefeaturehistory_population.DeviceFeatureHistoryPopulator;
import com.dstillery.exchangesimulator.util.UserAgentSampleProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Configuration
public class ExchangeSimulatorConfig {

    private BlockingQueue<String> bidderResponses = new ArrayBlockingQueue<String>(1000);
    private BlockingQueue<String> commonUsersBetweenBidderAndActionRecorder = new ArrayBlockingQueue<>(1000);

    @Autowired
    private DiscoveryService discoveryService;

    @Autowired
    private MetricReporterService metricReporterService;

    @Autowired
    private String appName;

    private String dataMasterUrl = ""; // we set this to empty. so discoveryService, will do its job of getting dataMaster url each time

    @Bean
    public SegmentCacheService segmentCacheService() {
        return new SegmentCacheService(
                discoveryService,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public TargetGroupCacheService targetGroupCacheService() {
        return new TargetGroupCacheService(discoveryService, dataMasterUrl, metricReporterService, appName);
    }

    @Bean
    public CacheService<Interest> interestCacheService() {
        return new CacheService<>(
                discoveryService,
                Interest.class,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public TargetGroupBWListCacheService targetGroupBWListCacheService(
            TargetGroupCacheService targetGroupCacheService) {
        return new TargetGroupBWListCacheService(
                discoveryService,
                targetGroupCacheService,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public ExchangeIdService exchangeIdService(
            NomadiniIdToExchangeIdsMapCassandraService nomadiniIdToExchangeIdsMapCassandraService,
            ConfigService configService) {
        ExchangeIdService exchangeIdService = new ExchangeIdService(
                nomadiniIdToExchangeIdsMapCassandraService,
                configService.getAsInt("numberOfSyncedIdsToRead")
        );

        exchangeIdService.setup();
        //reading the synced ids for the first time
        new Thread(exchangeIdService::refreshSyncedIdsFromMySql).start();

        return exchangeIdService;
    }

    @Bean
    public UserAgentSampleProvider userAgentSampleProvider() {
        List<String> userAgentSamples = new ArrayList<>(FileNomadiniUtil.readFileLinesFromResources("UserAgentSamples.txt"));
        return new UserAgentSampleProvider(userAgentSamples);
    }

    @Bean
    public BidRequestCreator bidRequestCreator(
            UserAgentSampleProvider userAgentSampleProvider) {
        return new BidRequestCreator(userAgentSampleProvider);
    }

    @Bean
    public BidRequestEnricher bidRequestEnricher(
            MySqlGlobalWhiteListService mySqlGlobalWhiteListService,
            TargetGroupBWListCacheService targetGroupBWListCacheService,
            ExchangeIdService exchangeIdService,
            UserAgentSampleProvider userAgentSampleProvider
    ) throws IOException, SQLException {
        return new BidRequestEnricher(
                mySqlGlobalWhiteListService,
                targetGroupBWListCacheService,
                userAgentSampleProvider,
                exchangeIdService
        );
    }

    @Bean
    public ActionRecorderLoadTester actionRecorderLoadTester(
            ExchangeIdService exchangeIdService,
            MySqlPixelService mySqlPixelService,
            MetricReporterService metricReporterService,
            ConfigService configService) {

        return new ActionRecorderLoadTester(
                metricReporterService,
                mySqlPixelService,
                exchangeIdService,
                commonUsersBetweenBidderAndActionRecorder,
                discoveryService);
    }

    @Bean
    public AdServerLoadTester adServerLoadTester(
            ConfigService configService,
            AeroCacheService< EventLog > realTimeEventLogCacheService,
            MetricReporterService metricReporterService) {

        return new AdServerLoadTester(
                configService,
                metricReporterService,
                discoveryService,
                realTimeEventLogCacheService);
    }

    @Bean
    public BidderLoadTester bidderLoadTester(
            ConfigService configService,
            BidRequestEnricher bidRequestEnricher,
            BidRequestCreator bidRequestCreator,
            MetricReporterService metricReporterService) throws MalformedURLException {
        return new BidderLoadTester(
                bidRequestEnricher,
                bidRequestCreator,
                bidderResponses,
                configService,
                metricReporterService,
                "google",
                configService.getAsInt("bidder.numberOfRequestToHitInOneTask"),
                discoveryService,
                commonUsersBetweenBidderAndActionRecorder);
    }

    @Bean
    public DeviceFeatureHistoryPopulator deviceFeatureHistoryPopulator(
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
            MetricReporterService metricReporterService,
            ExchangeIdService exchangeIdService,
            ClientAppDataReloadService clientAppDataReloadService,
            CacheService<Interest> interestCacheService,
            ConfigService configService) {
        return new DeviceFeatureHistoryPopulator(
                deviceFeatureHistoryCassandraService,
                metricReporterService,
                exchangeIdService,
                interestCacheService,
                clientAppDataReloadService,
                configService);
    }

    @Bean
    public ExchangeSimulatorService exchangeSimulatorService(
            ConfigService configService,
            ActionRecorderLoadTester actionRecorderLoadTester,
            BidderLoadTester bidderLoadTester,
            AdServerLoadTester adServerLoadTester,
            MetricReporterService metricReporterService) throws IOException, SQLException {
        ExchangeSimulatorService exchangeSimulatorService =  new ExchangeSimulatorService(
                 bidderResponses,
                 configService,
                 actionRecorderLoadTester,
                 bidderLoadTester,
                 adServerLoadTester,
                 metricReporterService
        );

        exchangeSimulatorService.start();
        return exchangeSimulatorService;
    }
}

package com.dstillery.exchangesimulator.loadtester;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExchangeSimulatorEssentialConfig {
    @Bean
    public String appPropertyFileName() {
        return "exchangesimulator.properties";
    }
}

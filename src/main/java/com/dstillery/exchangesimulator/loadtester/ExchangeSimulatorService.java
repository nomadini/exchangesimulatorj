package com.dstillery.exchangesimulator.loadtester;

import com.dstillery.common.config.ConfigService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.exchangesimulator.actionrecorder.ActionRecorderLoadTester;
import com.dstillery.exchangesimulator.adserver.AdServerCallerTask;
import com.dstillery.exchangesimulator.adserver.AdServerLoadTester;
import com.dstillery.exchangesimulator.bidder.BidderLoadTester;
import com.dstillery.exchangesimulator.util.SiteTestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExchangeSimulatorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeSimulatorService.class);

    private MetricReporterService metricReporterService;
    private BlockingQueue<String> bidderResponses;
    private BidderLoadTester bidderLoadTester;
    private ActionRecorderLoadTester actionRecorderLoadTester;
    private AdServerLoadTester adServerLoadTester;
    private ConfigService configService;

    public ExchangeSimulatorService(
            BlockingQueue<String> bidderResponses,
            ConfigService configService,
            ActionRecorderLoadTester actionRecorderLoadTester,
            BidderLoadTester bidderLoadTester,
            AdServerLoadTester adServerLoadTester,
            MetricReporterService metricReporterService) {

        this.configService = configService;
        this.metricReporterService = metricReporterService;
        this.actionRecorderLoadTester = actionRecorderLoadTester;
        this.adServerLoadTester = adServerLoadTester;
        this.bidderLoadTester = bidderLoadTester;
        this.bidderResponses = bidderResponses;

    }

    private OverallSystemLoadTester createOverallLoadTester(){
        return new OverallSystemLoadTester(bidderLoadTester);
    }

    private AdServerCallerTask createAdServerCaller() {
        return new AdServerCallerTask(
                bidderResponses,
                 metricReporterService,
                 adServerLoadTester
        );
    }

    public void start() {

        SiteTestHelper.populateDomainList();

        Boolean actionRecorderEnableLoadTestProp =
                configService.getAsBoolean("actionRecorder.enableLoadTest");

        Boolean bidderEnableLoadTestProp =
                configService.getAsBoolean("bidder.enableLoadTest");

        Boolean loadTestAdServerProp =
                configService.getAsBoolean("adServer.enableLoadTesting");

        LOGGER.debug("starting the exchange simulator");

        startActionRecorderLoader(actionRecorderEnableLoadTestProp,
                                  configService.getAsInt("numberOfTasksToLoadActionRecorder"));

        startAdServerLoader(loadTestAdServerProp);

        if (bidderEnableLoadTestProp) {
            startBidderRequestPreparerThreads();
            startAdserverThreads();
            startBidderThreads();
        } else {
            LOGGER.info("not loading bidder loader");
        }
    }

    private void startAdServerLoader(Boolean loadTestAdServerProp) {
        if (loadTestAdServerProp) {
            int numberOfAdServerLoadTester = 1;
            ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(numberOfAdServerLoadTester);

            for (int i=0; i < numberOfAdServerLoadTester; i++) {
                threadPool.scheduleAtFixedRate(() -> {
                    try {
                        adServerLoadTester.startLoadTestingAdServer();
                    } catch (IOException e) {
                        LOGGER.warn("exception occurred", e);
                    }
                }, 0 , 1, TimeUnit.MINUTES);
            }
        } else {
            LOGGER.debug("not load testing adserver based on properties set");
        }
    }

    private void startActionRecorderLoader(Boolean actionRecorderEnableLoadTestProp,
                                           int numberOfTasksToLoadActionRecorder) {
        if (actionRecorderEnableLoadTestProp) {

            ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(numberOfTasksToLoadActionRecorder);
            for (int i=0; i < numberOfTasksToLoadActionRecorder; i++) {
                threadPool.scheduleAtFixedRate(() -> {
                    try {
                        actionRecorderLoadTester.syncGoogleThread();
                    } catch (Exception e) {
                        LOGGER.warn("exception : {} ", e);
                    }
                }, 0, configService.getAsInt("actionRecorder.loadIntervalInSeconds"), TimeUnit.SECONDS);
            }


            ScheduledExecutorService threadPool2 = Executors.newScheduledThreadPool(numberOfTasksToLoadActionRecorder);
            for (int i=0; i < numberOfTasksToLoadActionRecorder; i++) {
                threadPool2.scheduleAtFixedRate(() -> {
                    try {
                        //50% of devices must be common with bidders so we have positive samples and negative samples for modeling
                        actionRecorderLoadTester.sendPixelActionToActionRecorder(10, RandomUtil.sudoRandomNumber(100) >= 50);
                    } catch (Exception e) {
                        LOGGER.warn("exception : {} ",  e);
                    }
                }, 0 , configService.getAsInt("actionRecorder.loadIntervalInSeconds"), TimeUnit.SECONDS);
            }
        }
    }

    private void startBidderThreads() {
        OverallSystemLoadTester overallSystemLoadTester = createOverallLoadTester();
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(1);
        threadPool.scheduleAtFixedRate(() -> {
            try {
                ExecutorService pool = Executors.newCachedThreadPool();
                List<Future<?>> allTasks = new ArrayList<>();
                //INCREASE the number of tasks to increase the load on bidder
                for (int i=0 ; i <  configService.getAsInt("bidder.numberOfTasksToLoadApp"); i++) {
                    Future<?> task = pool.submit(overallSystemLoadTester);
                    allTasks.add(task);
                }
                allTasks.forEach(task -> {
                    try {
                        task.get();
                    } catch (InterruptedException | ExecutionException e) {
                        LOGGER.warn("exception occurred", e);
                    }
                });
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }, 0 , configService.getAsInt("bidder.loadIntervalInSeconds"), TimeUnit.SECONDS);

    }
    private void startBidderRequestPreparerThreads() {
        ScheduledExecutorService threadPool2 = Executors.newScheduledThreadPool(1);
        threadPool2.scheduleAtFixedRate(() -> {
            try {
                bidderLoadTester.prepareRequests();
            } catch (Exception e) {
                LOGGER.warn("exception in scheduling", e);
            }
        }, 0 , 1, TimeUnit.SECONDS);
    }

    private void startAdserverThreads() {
        ScheduledExecutorService threadPool3 = Executors.newScheduledThreadPool(1);
        threadPool3.scheduleAtFixedRate(() -> {
            try {
                ExecutorService pool = Executors.newCachedThreadPool();
                //INCREASE the number of tasks to increase the load on bidder
                AdServerCallerTask adServerCallerTask = createAdServerCaller();
                pool.submit(adServerCallerTask).get();
                pool.shutdownNow();
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }, 0 , configService.getAsInt("adServer.loadIntervalInSeconds"), TimeUnit.SECONDS);

    }

}


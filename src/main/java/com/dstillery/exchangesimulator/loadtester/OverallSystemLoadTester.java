package com.dstillery.exchangesimulator.loadtester;

import com.dstillery.exchangesimulator.bidder.BidderLoadTester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OverallSystemLoadTester implements Runnable  {
    private static final Logger LOGGER = LoggerFactory.getLogger(OverallSystemLoadTester.class);

    private BidderLoadTester bidderLoadTester;

    public OverallSystemLoadTester(BidderLoadTester bidderLoadTester) {
        this.bidderLoadTester = bidderLoadTester;
    }

    public void run() {
        try {
            bidderLoadTester.sendRequestsToBidder();
        } catch (InterruptedException e) {
            LOGGER.warn("exception occurred", e);
        }
    }

}

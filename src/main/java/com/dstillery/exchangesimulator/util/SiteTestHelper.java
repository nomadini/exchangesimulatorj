package com.dstillery.exchangesimulator.util;

import com.dstillery.openrtb.beans.Site;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.common.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class SiteTestHelper {

    private static Map<String,Site> mapOfSites = new ConcurrentHashMap<>();
    private static List<String> sampleDomains = new ArrayList<>();

    public static Site createOrGetSite(String iabCategory,
                                       String domain) {
        assertAndThrow (!iabCategory.isEmpty());
        assertAndThrow (!domain.isEmpty());
        Site site = new Site ();
        String keyOfSiteInMap = iabCategory + domain;

        site.setId(StringUtil.hashStringBySha1(keyOfSiteInMap));
        site.getCat().add(iabCategory);

        site.setDomain(domain);
        site.setPage(getRandomPageForDomain(domain));

        getMapOfSites().put(keyOfSiteInMap, site);
//         MLOG (3) "created this site : {} ",  site.toJson ();

        return site;
    }

    public static void populateDomainList() {
        getSampleDomains().clear ();
        getSampleDomains().add ("bbcpersian.com");
        getSampleDomains().add ("amazon.com");
        getSampleDomains().add ("expeida.com");
        getSampleDomains().add ("farsnews.com");
        getSampleDomains().add ("sugarbear.com");
        getSampleDomains().add ("halloween.com");
        getSampleDomains().add ("fifaworldcup.com");
        getSampleDomains().add ("toothbrush.com");
        getSampleDomains().add ("nyc.com");
        getSampleDomains().add ("englishpremierleague.com");
        getSampleDomains().add ("candybar.com");
        getSampleDomains().add ("foxnews.com");
        getSampleDomains().add ("clickhole.com");
        getSampleDomains().add ("onions.net");
        getSampleDomains().add ("boost.org");
        getSampleDomains().add ("sony.net");
        getSampleDomains().add ("dstillery.com");
        getSampleDomains().add ("infindo.net");
        getSampleDomains().add ("admarketplace.com");
        getSampleDomains().add ("impressiveBodies.com");
        getSampleDomains().add ("askme.com");
        getSampleDomains().add ("www.aestheticnihilismenigma.com");
        getSampleDomains().add ("www.iran4offer01.com");
        getSampleDomains().add ("www.golden100Offer01.com");

    }

    private static List<String> getSampleDomains() {
        return sampleDomains;
    }

    public static String getRandomIABCategory() {
        return "IAB" + StringUtil.toStr(RandomUtil.sudoRandomNumber (20));
    }

    String getRandomIABSubCategory() {

        String iabCat = "IAB" +
                StringUtil.toStr(RandomUtil.sudoRandomNumber (20)) + "-"
                + StringUtil.toStr(RandomUtil.sudoRandomNumber (10));
        return iabCat;

    }

    static String getRandomDomainFromFiniteSet() {
//         //MLOG (2) << "getSampleDomains().size() is ", getSampleDomains().size();

        if (getSampleDomains().isEmpty()) {
            populateDomainList ();

        }
        String str;
        int rand = RandomUtil.sudoRandomNumber (getSampleDomains().size()).intValue() - 1;
        str = StringUtil.toStr(getSampleDomains().get(rand));
//         //MLOG (2) << "random web site is ", str;
        assertAndThrow (!str.isEmpty());
        return str;

    }

    public static String getRandomDomain() {
        if (getSampleDomains().isEmpty()) {
            populateDomainList ();

        }
        Long randPercentage = RandomUtil.sudoRandomNumber (100);

        Long randNumber = RandomUtil.sudoRandomNumber (100);
        if (randNumber < randPercentage) {
            //returning the random domain from finite set
            return StringUtil.toStr(
                    getSampleDomains().get(
                            RandomUtil.sudoRandomNumber (
                                    getSampleDomains().size() - 1).intValue()));
        } else {
            //returning a totally random domain
            return StringUtil.randomString(6) + ".com";
        }
    }
    static String getRandomPageForDomain(String domain) {

        return StringUtil.toStr("http://www.") + domain +
                StringUtil.toStr("/") +
                StringUtil.randomString(4) +
                StringUtil.toStr(".html");
    }

    static String getRandomPageFromDomain() {
        String dash = "-";
        String val = StringUtil.toStr(RandomUtil.sudoRandomNumber (1000)) + dash + getRandomDomainFromFiniteSet ();
        assertAndThrow (!val.isEmpty());
        return val;
    }

    public static Map<String, Site> getMapOfSites() {
        return mapOfSites;
    }
}

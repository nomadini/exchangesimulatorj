package com.dstillery.exchangesimulator.util;

import com.dstillery.common.util.RandomUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserAgentSampleProvider {
    private List<String> userAgentSamples = new ArrayList<>();
    
    public List<String> getUserAgentSamples() {
        return userAgentSamples;
    }

    public UserAgentSampleProvider(List<String> userAgentSamples) {
        this.userAgentSamples = userAgentSamples;
    }

    public String getRandomUserAgentFromFiniteSet() throws IOException {
        return getUserAgentSamples().get(RandomUtil.sudoRandomNumber (getUserAgentSamples().size() - 1).intValue() );
    }
}

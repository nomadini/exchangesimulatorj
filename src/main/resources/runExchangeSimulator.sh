#!/usr/bin/env bash
APP_NAME=exchangesimulator
HEAT_SIZE_MB=256
JMX_PORT=1022
MAIN_CLASS=com.dstillery.exchangeSimulator.loadtester.ExchangeSimulator
APP_OPTS=""
JAR_PATH_NAME=~/.m2/repository/com/exchangesimulator-mt/1.0-SNAPSHOT/exchangesimulator-mt-1.0-SNAPSHOT.jar
FINAL_RELEASE_NAME=~/exchangesimulator-1.0-SNAPSHOT.jar
PROJECT_DIR=/Users/mtaabodi/Documents/workspace-mt/exchangesimulatorj

BASEDIR=$(dirname "$0")
source /Users/mtaabodi/Documents/workspace-mt/config/run.sh
